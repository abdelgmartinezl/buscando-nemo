package info.nemo.buscando;

import java.util.Random;

public enum Especie {
    COJINUA,
    BAGRE,
    ROBALO,
    CORVINA,
    TILAPIA,
    SARGENTO,
    PARGO,
    TIBURON,
    SALMON,
    BACALAO,
    NEMO2;

    public static Especie getEspecie() {
        Random random = new Random();
        return values()[random.nextInt(values().length)];
    }
}
