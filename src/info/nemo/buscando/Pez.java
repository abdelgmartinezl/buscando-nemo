package info.nemo.buscando;

public class Pez {
    private double peso;
    private Especie especie;

    public Pez() {
        this.peso = Math.random()*101;
        this.especie = Especie.getEspecie();
    }

    public double getPeso() {
        return peso;
    }

    public String getEspecie() {
        return especie.toString();
    }
}
